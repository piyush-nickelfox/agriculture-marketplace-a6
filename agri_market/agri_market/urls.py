from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('home/', include('apps.home.urls')),
    path('', include('apps.agriculture.urls')),
    path('auth/', include('apps.member.urls')),
    path('admin/', admin.site.urls),
]
