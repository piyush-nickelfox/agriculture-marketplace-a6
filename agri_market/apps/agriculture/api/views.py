from .serializers import (
    AdminLoanUpdateSerializer, 
    BidSerializers, 
    BidStatusSerializers, 
    BidUpdateSerializers, 
    CategorySerializers, 
    CommoditySerializers, 
    CropSerializers, 
    LoanSerializers, 
    MarketSerializers, 
    RatingUpdateSerializers, 
    UserOrderSerializers, 
    AdminOrderSerializers, 
    FarmerSerializers, 
    FarmSerializers, 
    RatingSerializers
)
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework.permissions import IsAuthenticated
from .permissions import AdminPermissions, AdminRatingPermission, FarmPermissions, FarmerAdminPermission, FarmerAgentPermission, FarmerPermissions, GetPermissionOnly, OrderPermissions, FarmerCropPermissions, farmer_role, admin_role
from rest_framework import views, status, generics
from apps.agriculture import models
from apps.member.models import User
from apps.home.models import Role
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError
from django.db.models import Avg


class CategoryView(views.APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated & AdminPermissions]

    def get(self, request, id=None, format=None):
        if id is not None:
            category = models.get_instance(models.Category,{'pk':id})
            serializer = CategorySerializers(category)
        else:    
            category = models.Category.objects.all()
            serializer = CategorySerializers(category, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = CategorySerializers(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'Message':'Category is added successfully.'}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        
    def patch(self, request, id=None, format=None):
        if id is not None:
            category_obj = models.get_instance(models.Category,{'pk':id})
            serializer =  CategorySerializers(category_obj, data=request.data, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response({'Message':'Category updated successfully.'}, status=status.HTTP_200_OK)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response({'Error','No such category exists.'}, status=status.HTTP_404_NOT_FOUND)
        

class CommodityView(views.APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated & AdminPermissions]

    def get(self, request, id=None, format=None):
        if id is not None:
            commodity = models.get_instance(models.Commodity,{'pk':id})
            serializer = CommoditySerializers(commodity)
        else:
            commodity = models.Commodity.objects.all()
            serializer = CommoditySerializers(commodity, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = CommoditySerializers(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'Commodity is added successfully.'}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        
    def patch(self, request, id=None, format=None):
        if id is not None:
            commodity_obj = models.get_instance(models.Commodity,{'pk':id})
            serializer =  CommoditySerializers(commodity_obj, data=request.data, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response({'Commodity updated successfully.'}, status=status.HTTP_200_OK)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response({'No such commodity exists.'}, status=status.HTTP_404_NOT_FOUND)
        

class UserOrderView(views.APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated & OrderPermissions]

    def get(self, request, format=None):
        order = models.Order.objects.all()
        serializer = UserOrderSerializers(order, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        request.data['farmer'] = request.user.id
        item = request.data['item']
        commodity = models.get_instance(models.Commodity,{'pk':item})
        price = commodity.cost 
        if not isinstance(request.data['quantity'], int):
            raise ValidationError({'Please enter valid quantity.'})
        request.data['price'] = request.data['quantity'] * price
        serializer = UserOrderSerializers(data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'Order placed successfully.'}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AdminOrderView(generics.RetrieveUpdateAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated & OrderPermissions]
    queryset = models.Order.objects.all()
    serializer_class = AdminOrderSerializers
  

class FarmerView(views.APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated & GetPermissionOnly]

    def get(self, request, id=None, format=None):
        if id is not None:
            farmer = User.get_instance({'id':id, 'is_active':True, 'is_approved':True})
            serializer = FarmerSerializers(farmer, context={'request': request})
        else:
            role = Role.get_instance({'name':'farmer'}).id
            farmer = User.filter_instance({'role':role, 'is_active':True, 'is_approved':True})
            serializer = FarmerSerializers(farmer, many=True, context={'request': request})
        return Response(serializer.data)


class RatingView(views.APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated & AdminRatingPermission]

    def get(self, request, id=None, format=None):
        data={}
        if id is not None:
            rating = models.filter_instance(models.Rating, {'farmer':id}).aggregate((Avg('rating')))
            data['farmer_avg_rating'] = rating['rating__avg']
        if request.user.role.id == Role.get_instance({'name':'admin'}).id:
            admin_rate = models.filter_instance(models.Rating, {'admin': request.user.id, 'farmer':id})
            if admin_rate.exists():
                serializer = RatingSerializers(admin_rate, many=True)
                data['farmer_my_rate'] = serializer.data
            else:
                data['farmer_my_rate'] = "You haven't given rating"
        return Response(data)

    def post(self, request, id=None, format=None):
        if id is not None:
            request.data['farmer'] = id
            request.data['admin'] = request.user.id
            serializer = RatingSerializers(data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response({'Thank you for your rating.'}, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response('No such farmer exists.', status=status.HTTP_404_NOT_FOUND)

    def patch(self, request, id=None, format=None):
        if id is not None:
            rating = models.get_instance(models.Rating,{'farmer':id, 'admin':request.user.id})
            serializer = RatingUpdateSerializers(rating, data=request.data, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response({'Rating updated.'}, status=status.HTTP_200_OK)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response({'No such farmer exists.'}, status=status.HTTP_404_NOT_FOUND)


class FarmerFarmView(views.APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated & GetPermissionOnly]

    def get(self, request, id=None, id2=None,format=None):
        if id is not None:
            if id2 is not None:
                farm = models.get_instance(models.Farm, {'id':id2})
                serializer = FarmSerializers(farm)
            else:
                farm = models.filter_instance(models.Farm, {'farmer':id})
                serializer = FarmSerializers(farm, many=True)
            return Response(serializer.data)
        return Response('No such farmer exists', status=status.HTTP_404_NOT_FOUND)


class FarmView(generics.ListCreateAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated & FarmPermissions]

    def get(self, request, format=None):
        farmer = request.user.id       
        farm = models.filter_instance(models.Farm, {'farmer':farmer})
        serializer = FarmSerializers(farm, many=True)   
        return Response(serializer.data)
        
    def post(self, request, format=None):
        request.data['farmer'] = request.user.id
        serializer = FarmSerializers(data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'New farm is added successfully.'}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class FarmDetailView(generics.RetrieveUpdateAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated & FarmPermissions]
    queryset = models.Farm.objects.all()
    serializer_class = FarmSerializers


class FarmCropView(views.APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated & FarmerCropPermissions]

    def get(self, request, id=None,format=None):
        if id is not None:
            farm = models.filter_instance(models.Crop, {'farm':id})
            serializer = CropSerializers(farm, many=True)
            return Response(serializer.data)
        return Response('No such farm exists', status=status.HTTP_404_NOT_FOUND)
        
    def post(self, request, id=None, format=None):
        if id is not None:
            request.data['farm'] = id
            serializer = CropSerializers(data=request.data, context={'request':request})
            if serializer.is_valid():
                serializer.save()
                return Response({'New farm is added successfully.'}, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CropDetailView(generics.RetrieveUpdateAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated & FarmerCropPermissions]
    
    def get_object(self, id=None, id2=None):
        if id is not None:
            if id2 is not None:
                crop = models.get_instance(models.Crop, {'id':id2, 'farm':id})
                self.check_object_permissions(self.request, crop)
                return crop
            return Response(status=status.HTTP_404_NOT_FOUND)
        return Response(status=status.HTTP_404_NOT_FOUND)

    def get(self, request, id=None, id2=None, format=None):
        crop = self.get_object(id, id2)
        serializer = CropSerializers(crop)
        return Response(serializer.data)
           
    def patch(self, request, id=None, id2=None, format=None):
        crop = self.get_object(id, id2)
        serializer = CropSerializers(crop, data=request.data, partial=True, context={'request':request})
        if serializer.is_valid():
            serializer.save()
            return Response({'Crop details updated.'}, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            

class MarketView(generics.ListCreateAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated & FarmerPermissions]
    
    def get(self, request, format=None):
        if request.user.role.id == farmer_role:
            market = models.filter_instance(models.Market,{'farmer':request.user.id})
            serializer = MarketSerializers(market, many=True)
        else:
            market = models.Market.objects.all()
            serializer = MarketSerializers(market, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        request.data['farmer'] = request.user.id
        serializer = MarketSerializers(data=request.data, context={'request':request})
        if serializer.is_valid():
            serializer.save()
            return Response({'Crop added in market.'}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class MarketUpdateView(generics.RetrieveUpdateAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated & FarmerPermissions]
    lookup_field = 'id'
    
    def get_object(self,id=None):
        if id is not None:
            market = models.get_instance(models.Market, {'id':id})
            self.check_object_permissions(self.request,market)
            return market

    def patch(self, request, id=None, format=None):
        market = self.get_object(id)
        request.data['farmer'] = request.user.id
        serializer = MarketSerializers(market, data=request.data, partial=True, context={'request':request})
        if serializer.is_valid():
            serializer.save()
            return Response({'Market crop updated.'}, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        

class LoanView(views.APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated & FarmerAdminPermission]

    def get(self, request, id=None, format=None):
        if id is not None:
            loan = models.get_instance(models.Loan, {'id':id})
            serializer = LoanSerializers(loan)
        else:
            if request.user.role.id == farmer_role:
                loan = models.filter_instance(models.Loan, {'farmer':request.user.id})
            else:
                loan = models.Loan.objects.all()
            serializer = LoanSerializers(loan, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, format=None):
        request.data['farmer'] = request.user.id
        serializer = LoanSerializers(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'Loan request submitted.'}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def patch(self, request, id=None, format=None):
        if id is not None:
            loan = models.get_instance(models.Loan, {'id':id})
            request.data['approved_by'] = request.user.id
            serializer = AdminLoanUpdateSerializer(loan, data=request.data, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response({'Loan request is updated.'}, status=status.HTTP_200_OK)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    

class BidView(views.APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated & FarmerAgentPermission]

    def get(self, request, format=None):
        if request.user.role.id == farmer_role:
            bid = models.filter_instance(models.Bid, {'farmer':request.user.id})
        else:
            bid = models.filter_instance(models.Bid, {'agent':request.user.id})
        serializer = BidSerializers(bid, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, format=None):
        request.data['agent'] = request.user.id
        item = models.get_instance(models.Market, {'id':request.data['item']})
        request.data['farmer'] = item.farmer.id
        serializer = BidSerializers(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'Loan request submitted.'}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class BidDetailView(generics.RetrieveUpdateAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated & FarmerAgentPermission]
    lookup_field = 'id'

    def get_object(self,id=None):
        if id is not None:
            bid = models.get_instance(models.Bid, {'id':id})
            self.check_object_permissions(self.request, bid)
            return bid

    def patch(self, request, id=None, format=None):
        bid = self.get_object(id)
        user_role = request.user.role.id
        if user_role == farmer_role:
            serializer = BidStatusSerializers(bid, data=request.data, partial=True)
        else:
            serializer = BidUpdateSerializers(bid, data=request.data, partial=True)

        if serializer.is_valid():
            serializer.save()
            return Response({'Bid updated.'}, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        

