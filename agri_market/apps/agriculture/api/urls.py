
from django.urls import path, include

from . import views

urlpatterns = [
    #path('',include(router.urls)),

    path('category/', views.CategoryView.as_view(), name='category'),
    path('category/<uuid:id>/', views.CategoryView.as_view(), name='category-detail'),
    
    path('commodity/', views.CommodityView.as_view(), name='commodity'),
    path('commodity/<uuid:id>/', views.CommodityView.as_view(), name='commodity-detail'),

    path('order/', views.UserOrderView.as_view(), name='order'),
    path('order/<uuid:pk>/', views.AdminOrderView.as_view(), name='order-detail'),

    path('farmer/', views.FarmerView.as_view(), name='farmer'),
    path('farmer/<uuid:id>/', views.FarmerView.as_view(), name='farmer-detail'),

    path('farmer/<uuid:id>/rating/', views.RatingView.as_view(), name='farmer-rating'),
    
    path('farmer/<uuid:id>/farm/', views.FarmerFarmView.as_view(), name='farmerfarm'),
    path('farmer/<uuid:id>/farm/<uuid:id2>', views.FarmerFarmView.as_view(), name='farmerfarm-detail'),
    
    path('farm/', views.FarmView.as_view(), name='farm'),
    path('farm/<uuid:id>/', views.FarmDetailView.as_view(), name='farm-detail'),

    path('farm/<uuid:id>/crop/', views.FarmCropView.as_view(), name='farmcrop'),
    path('farm/<uuid:id>/crop/<uuid:id2>/', views.CropDetailView.as_view(), name='farmcrop-detail'),

    path('market/', views.MarketView.as_view(), name='market'),
    path('market/<uuid:id>/', views.MarketUpdateView.as_view(), name='market-detail'),

    path('loan/', views.LoanView.as_view(), name='loan'),
    path('loan/<uuid:id>/', views.LoanView.as_view(), name='loan-detail'),
    
    path('bid/', views.BidView.as_view(), name='bid'),
    path('bid/<uuid:id>/', views.BidDetailView.as_view(), name='bid-detail'),

]