from email import message
from urllib import request
from rest_framework import permissions
from apps.home.models import Role

admin_role = Role.get_instance({'name':'admin'}).id
farmer_role = Role.get_instance({'name':'farmer'}).id
agent_role = Role.get_instance({'name':'agent'}).id


class AdminPermissions(permissions.BasePermission):
    def has_permission(self, request, view):
        user = request.user
        if request.method in permissions.SAFE_METHODS: 
            return True
        else:
            if user.role.id == admin_role:
                return True
            else:
                return False


class OrderPermissions(permissions.BasePermission):  
    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS: 
            return True
        elif request.method == 'POST':
            return request.user.role.id == farmer_role
        return request.user.role.id == admin_role


class FarmPermissions(permissions.BasePermission):  
    def has_permission(self, request, view):
        return request.user.role.id == farmer_role
        
    def has_object_permission(self, request, view, obj):
        return obj.farmer == request.user


class GetPermissionOnly(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True
        return False


class AdminRatingPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True
        return request.user.role.id == admin_role


class FarmerCropPermissions(permissions.BasePermission):  
    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True
        return request.user.role.id == farmer_role
     
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        return obj.farm.farmer.id == request.user.id


class FarmerPermissions(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            print('x')
            return True
        print('y')
        return request.user.role.id == farmer_role

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            print('xx')
            return True
        print('yy')
        return obj.farmer.id == request.user.id
     
class FarmerAdminPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return request.user.role.id != agent_role
        elif request.method == 'POST':
            return request.user.role.id == farmer_role
        return request.user.role.id == admin_role

class FarmerAgentPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.method == 'POST':
            return request.user.role.id == agent_role
        return request.user.role.id != admin_role
    
    def has_object_permission(self, request, view, obj):
        if request.user.role.id == agent_role:
            obj.agent.id == request.user.id
        return obj.farmer.id == request.user.id