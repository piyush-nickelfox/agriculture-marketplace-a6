from django.forms import ValidationError
from rest_framework import serializers
from apps.member.models import User
from apps.agriculture import models
import datetime

class CategorySerializers(serializers.ModelSerializer):
    class Meta:
        model = models.Category
        fields = ['id','name','type']

    def validate(self, args):
        if models.filter_instance(self.Meta.model, args).exists():
            raise serializers.ValidationError('This category already exists.')
        return super().validate(args)


class CommoditySerializers(serializers.ModelSerializer):
    class Meta:
        model = models.Commodity
        fields = ['id','name','description','category','cost']

    def validate(self, args):
        if models.filter_instance(self.Meta.model, args).exists():
            raise serializers.ValidationError('Already exists.')
        return super().validate(args)


class UserOrderSerializers(serializers.ModelSerializer):
    class Meta:
        model = models.Order
        fields = ['id','item','farmer','quantity', 'price','is_delivered','deliver_date'] 
        read_only_fieldss = ['id','is_delivered','delivered_date']

    def validate_amount(self, amount):
        if amount<1000:
            raise serializers.ValidationError('Cannot request for amount less than 1000.')
        return amount
    
    def validate_quantity(self, value):
        if value <= 0:
            raise serializers.ValidationError('Please enter valid quantity.')
        return value

    
class AdminOrderSerializers(serializers.ModelSerializer):
    class Meta:
        model = models.Order
        fields = ['id','item','farmer','quantity', 'price','is_delivered','deliver_date'] 
        read_only_fieldss = ['id','item','farmer','quantity', 'price']

    def validate_deliver_date(self, date):
        if date > datetime.date.today():
            raise serializers.ValidationError('Please enter a valid date.')
        return date


class RatingSerializers(serializers.ModelSerializer):
    class Meta:
        model = models.Rating
        fields = ['id','admin','farmer','rating']

    def validate_rating(self, rating):
        if rating<1 or rating>10:
            raise serializers.ValidationError('Please enter valid rating.')
        return rating
    
    def validate(self, args):
        print(args)
        if models.filter_instance( 
            models.Rating, {
                'admin': args.get('admin', None), 
                'farmer':args.get('farmer', None)}
            ).exists():
            raise serializers.ValidationError('You already given rating. Please update your rating.')
        return super().validate(args)


class RatingUpdateSerializers(serializers.ModelSerializer):
    class Meta:
        model = models.Rating
        fields = ['id','admin','farmer','rating']
        read_only_fieldss = ['id', 'admin','farmer']

    def validate_rating(self, rating):
        if rating<1 or rating>10:
            raise serializers.ValidationError('Please enter valid rating.')
        return rating


class FarmerSerializers(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id','name','email']


# class MediaSerializers(serializers.ModelSerializer):
#     class Meta:
#         model = models.Media
#         fields = ['id','iamge']


class FarmSerializers(serializers.ModelSerializer):
    class Meta:
        model = models.Farm
        fields = ['id','farmer','length','breadth','purchased_on','soil']

    def validate_soil(self, soil):
        if not models.filter_instance(models.Category, {'id':soil.id, 'type':'Soil'}).exists():
            raise serializers.ValidationError('Please enter valid soil type')
        return soil

    def validate(self, attrs):
        length = attrs.get('length')
        breadth = attrs.get('breadth')
        if length<=0 or breadth<=0:
            raise serializers.ValidationError('Please enter valid length/breadth in feet.')
        return super().validate(attrs)

    def validate_purchased_on(self, date):
        if date >= datetime.date.today():
            raise serializers.ValidationError('Please enter a valid date.')
        return date


class CropSerializers(serializers.ModelSerializer):
    class Meta:
        model = models.Crop
        fields = ['id','farm','name','type','status']

    def validate_farm(self, farm):
        farmer = self.context['request'].user
        if not models.filter_instance(models.Farm, {'id':farm.id,'farmer':farmer.id}).exists():
            raise serializers.ValidationError('Please enter valid farm.')
        return farm
    
    def validate_type(self, crop):
        if not models.filter_instance(models.Category, {'id':crop.id, 'type':'Crop'}).exists():
            raise serializers.ValidationError('Please enter valid crop type')
        return crop


class MarketSerializers(serializers.ModelSerializer):
    class Meta:
        model = models.Market
        fields = ['id','farmer','crop','quantity','admin_cost','agent_cost','is_sold','sell_date','expire_date']
        read_only_fields = ['id']

    def validate_crop(self, crop):
        farmer = self.context['request'].user
        if not models.filter_instance(models.Crop, {'id': crop.id}).exists():
            raise serializers.ValidationError('Please enter valid crop.')
        if models.get_instance(models.Crop,{'id':crop.id}).farm.farmer.id != farmer.id:
            raise serializers.ValidationError('Please add your crop only.')
        return crop

    def validate_quantity(self, quantity):
        if quantity<0:
            raise serializers.ValidationError('Please enter correct quantity')  
        return quantity

    def validate_expire_date(self, date):
        if date<=datetime.date.today():
            raise serializers.ValidationError('Please enter non expire products in market.')
        return date

    def validate(self, args):
        if args.get('admin_cost')<=0 or args.get('agent_cost')<=0:
            raise serializers.ValidationError('Enter valid admin/agent cost.')
        return super().validate(args)


class LoanSerializers(serializers.ModelSerializer):
    class Meta:
        model = models.Loan
        fields = ['id','farmer','reason','amount','rate','tenure','is_approved','approved_by']
        read_only_fields = ['id','rate','tenure','is_approved','approved_by']

class AdminLoanUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Loan
        fields = ['id','farmer','reason','amount','rate','tenure','is_approved','approved_by']
        read_only_fields = ['id','farmer','reason','amount']

class BidSerializers(serializers.ModelSerializer):
    class Meta:
        model = models.Bid
        fields = ['id','agent','farmer','item','quantity','cost','is_finalized']
        read_only_fields = ['id','is_finalized']

    def validate_quantity(self, value):
        if value <= 0:
            raise serializers.ValidationError('Please enter valid quantity.')
        return value

    def validate(self, args):
        item = args.get('item')
        agent = args.get('agent')
        if models.filter_instance(models.Bid, {'item':item.id, 'agent':agent.id}).exists():
            raise serializers.ValidationError('Already Bidded. Please update your bid.')
        return super().validate(args)

class BidUpdateSerializers(serializers.ModelSerializer):
    class Meta:
        model = models.Bid
        fields = ['id','agent','farmer','item','quantity','cost','is_finalized']
        read_only_fields = ['id','agent','farmer','item','is_finalized']
    
    def validate_quantity(self, value):
        if value <= 0:
            raise serializers.ValidationError('Please enter valid quantity.')
        return value

class BidStatusSerializers(serializers.ModelSerializer):
    class Meta:
        model = models.Bid
        fields = ['id','agent','farmer','item','quantity','cost','is_finalized']
        read_only_fields = ['id','agent','farmer','item','quantity','cost']

class MarketOrderSerializers(serializers.ModelSerializer):
    class Meta:
        model = models.MarketOrder
        fields = ['id','item','user','quantity','cost','is_delivered','deliver_date']


