# Generated by Django 3.1.5 on 2022-03-14 18:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agriculture', '0007_auto_20220313_1409'),
    ]

    operations = [
        migrations.AlterField(
            model_name='loan',
            name='rate',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=10),
        ),
        migrations.AlterField(
            model_name='loan',
            name='tenure',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=10),
        ),
    ]
