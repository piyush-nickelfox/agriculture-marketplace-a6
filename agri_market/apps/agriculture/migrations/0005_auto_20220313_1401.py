# Generated by Django 3.1.5 on 2022-03-13 08:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agriculture', '0004_auto_20220312_0145'),
    ]

    operations = [
        migrations.AlterField(
            model_name='loan',
            name='tenure',
            field=models.DecimalField(decimal_places=2, max_digits=10),
        ),
    ]
