from tkinter.tix import Tree
from django.db import models
from apps.home.models import BaseModel
from apps.member.models import User
from rest_framework.exceptions import ValidationError

@staticmethod
def get_instance(model, data):
    try:
        return model.objects.get(**data)
    except:
        raise ValidationError('No data exists.')

@staticmethod
def get_instance(model, data):
    try:
        return model.objects.get(**data)
    except:
        raise ValidationError('No data exists.')

@staticmethod
def create_instance(model, data):
    try:
        return model.objects.create(**data)
    except:
        raise ValidationError('No data exists.')

@staticmethod
def filter_instance(model, data):
    try:
        return model.objects.filter(**data)
    except:
        raise ValidationError('No data exists.')


class Category(BaseModel):
    class Meta:
        verbose_name = "Category"
        verbose_name_plural = "Categories"
    TYPE = (
        ('Crop', 'Crop'),
        ('Soil','Soil')
    )
    name = models.CharField(max_length=255)
    type = models.CharField(max_length=10, choices=TYPE, default='Crop')

    def __str__(self):
        return self.name


class Commodity(BaseModel):
    class Meta:
        verbose_name = "Commodity"
        verbose_name_plural = "Commodities"
    CATEGORY = (
        ('Product','Product'),
        ('Service','Service')
    )
    name = models.CharField(max_length=255)
    description = models.TextField(default="Description of product/service.")
    category = models.CharField(max_length=10, choices=CATEGORY, default='Product')
    cost = models.DecimalField(max_digits=10, decimal_places=2)

    def __str__(self):
        return self.name


class Order(BaseModel):
    item = models.ForeignKey(Commodity, on_delete=models.CASCADE)
    farmer = models.ForeignKey(User, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField()
    price = models.DecimalField(max_digits=10, decimal_places=2)
    is_delivered = models.BooleanField(default=False)
    deliver_date = models.DateField(blank=True, null=True)
    
    def __str__(self):
        return f'{self.item} ordered by {self.farmer}'


class Media(BaseModel):
    image = models.ImageField(upload_to='farmImages')


class Farm(BaseModel):
    farmer = models.ForeignKey(User, on_delete=models.CASCADE)
    length = models.DecimalField(max_digits=10, decimal_places=2)
    breadth = models.DecimalField(max_digits=10, decimal_places=2)
    purchased_on = models.DateField()
    soil = models.ForeignKey(Category, on_delete=models.CASCADE)
    images = models.ManyToManyField(Media, blank=True, null=True)

    def __str__(self):
        return f'{self.farmer}s farm'


class Crop(BaseModel):
    CROP_STATUS_HISTROY = {
        'sowing': None,
        'seeding': None,
        'harvesting': None
    }
    farm = models.ForeignKey(Farm, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    type = models.ForeignKey(Category, on_delete=models.CASCADE)
    #sowing, seeding, harvesting
    status = models.JSONField(default=CROP_STATUS_HISTROY)
    
    def __str__(self):
        return self.name


class Rating(BaseModel):
    RATE_SCALE = tuple((i,i) for i in range(11))
    admin = models.ForeignKey(User, related_name='admin', on_delete=models.CASCADE)
    farmer = models.ForeignKey(User, related_name='farmer', on_delete=models.CASCADE)
    rating = models.IntegerField(choices=RATE_SCALE, default=0)
    
    def __str__(self):
        return f'{self.admin}-to-{self.farmer}-{self.rating}'


class Market(BaseModel):
    farmer = models.ForeignKey(User, on_delete=models.CASCADE)
    crop = models.ForeignKey(Crop, on_delete=models.CASCADE)
    quantity = models.IntegerField()
    admin_cost = models.DecimalField(max_digits=10, decimal_places=2)
    agent_cost = models.DecimalField(max_digits=10, decimal_places=2)
    is_sold = models.BooleanField(default=False)
    sell_date = models.DateField(blank=True, null=True)
    expire_date = models.DateField()
    
    def __str__(self):
        return f'{self.farmer}-crop-{self.crop}'


class Loan(BaseModel):
    approved_by = models.ForeignKey(User, related_name='approved_by', on_delete=models.CASCADE, blank=True, null=True)
    farmer = models.ForeignKey(User, related_name='farmerLoan', on_delete=models.CASCADE)
    reason = models.TextField()
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    rate = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    tenure = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    is_approved = models.BooleanField(default=False)
    
    def __str__(self):
        return f'{self.farmer}-loanOf-{self.amount}'


class Bid(BaseModel):
    agent = models.ForeignKey(User, related_name='agent', on_delete=models.CASCADE)
    farmer = models.ForeignKey(User, related_name='farmerBid', on_delete=models.CASCADE)
    item = models.ForeignKey(Market, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField()
    cost = models.DecimalField(max_digits=10, decimal_places=2)
    is_finalized = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.agent}-bid-{self.farmer}-iitem-{self.item}'


class MarketOrder(BaseModel):
    item = models.ForeignKey(Market, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField()
    cost = models.DecimalField(max_digits=10, decimal_places=2)
    is_deliverd = models.BooleanField(default=False)
    deliver_date = models.DateField(blank=True, null=True)
    

