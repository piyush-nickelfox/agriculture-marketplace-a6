from django.contrib import admin

from apps.agriculture import models

@admin.register(models.Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['id','name', 'type']

@admin.register(models.Commodity)
class CommodityAdmin(admin.ModelAdmin):
    list_display = ['id','name', 'category', 'cost']

@admin.register(models.Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ['id','item', 'farmer']

@admin.register(models.Farm)
class FarmAdmin(admin.ModelAdmin):
    list_display = ['id','farmer', 'length', 'breadth', 'soil']

@admin.register(models.Crop)
class CropAdmin(admin.ModelAdmin):
    list_display = ['id','farm', 'name', 'type','status']

@admin.register(models.Rating)
class RatingAdmin(admin.ModelAdmin):
    list_display = ['id','admin','farmer','rating']

@admin.register(models.Market)
class MarketAdmin(admin.ModelAdmin):
    list_display = ['id','farmer','crop','quantity','expire_date']

@admin.register(models.Loan)
class LoanAdmin(admin.ModelAdmin):
    list_display = ['id','farmer','amount','approved_by','is_approved']


@admin.register(models.Bid)
class BidAdmin(admin.ModelAdmin):
    list_display = ['id','farmer','agent','item','quantity','cost']

admin.site.register(models.MarketOrder)

