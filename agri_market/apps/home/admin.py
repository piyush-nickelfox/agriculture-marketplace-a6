from atexit import register
from django.contrib import admin

# Register your models here.
from .models import Role

@admin.register(Role)
class RoleAdmin(admin.ModelAdmin):
    list_display = ['id','name']