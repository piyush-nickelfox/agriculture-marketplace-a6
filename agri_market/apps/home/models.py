from django.db import models
import uuid
from rest_framework.exceptions import ValidationError
# Create your models here.
from django.contrib.auth.base_user import BaseUserManager

#---------Home------------------
class BaseModel(models.Model):
    
    class Meta:
        abstract = True

    id = models.UUIDField(primary_key=True, default = uuid.uuid4, unique = True, editable=False)
    created_at = models.DateTimeField(auto_now_add = True)
    updated_at = models.DateTimeField(auto_now = True)
    deleted_at = models.DateTimeField(blank = True, null = True)
    

#----------- User ----------------------------

class CustomUserManager(BaseUserManager):

    def create_user(self, email, name,  password, **extra_fields):
        if not email:
            raise ValueError('The Email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, name=name, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, name, password, **extra_fields):
      
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')
        return self.create_user(email, name, password, **extra_fields)


class Role(BaseModel):
    name = models.CharField(max_length = 6, unique=True, null=False, blank=False)

    def __str__(self):
        return self.name
    
    @staticmethod
    def get_instance(data):
        try:
            return Role.objects.get(**data)
        except:
            return ValidationError('Role does not exists.')
    
    @staticmethod
    def filter_instance(data):
        try:
            return Role.objects.filter(**data)
        except:
            return ValidationError('No such role exists.')