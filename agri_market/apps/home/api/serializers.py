from rest_framework import serializers
from apps.home.models import Role

class RoleSerializers(serializers.ModelSerializer):
    class Meta:
        model = Role
        fields = ['id', 'name']