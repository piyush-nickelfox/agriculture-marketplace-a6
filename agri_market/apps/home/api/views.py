from .serializers import RoleSerializers
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework import views, status
from apps.home.models import Role
from rest_framework.response import Response
from .permissions import IsAdminPermission
from django.contrib.gis.geoip2 import GeoIP2
import requests
from ipware import get_client_ip

class RoleView(views.APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated & IsAdminPermission]

    def get(self, request, id=None, format=None):
        if id is not None:
            role = Role.get_instance({'id': id})
            serializer = RoleSerializers(role)
        else:        
            roles = Role.objects.all().exclude(name='admin')
            serializer = RoleSerializers(roles, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = RoleSerializers(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'message':'New role is added successfully.'}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        
        
class WeatherApi(views.APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, format=None):
        client_ip, is_routable = get_client_ip(request)
        g = GeoIP2() 
        lat,lng = g.lat_lon(client_ip)
        print(lat, lng)
        #api_key = 'fb16a503786494d8b538a2207cd192f8'
        #url = f'api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lng}&appid={api_key}'
        print(client_ip, is_routable)
        #city_weather = requests.get(url).json() #request the API data and convert the JSON to Python data types
        #return Response({'weather_data':city_weather})
        return Response({'h':'city_weather'})
