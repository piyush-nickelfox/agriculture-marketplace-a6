from rest_framework import permissions
from apps.home.models import Role

admin_role = Role.objects.get(name='admin').id

class IsAdminPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        user = request.user
        if request.method in permissions.SAFE_METHODS: 
            return True
        elif request.method=='POST':
            return user.role.id == admin_role       
        return False
