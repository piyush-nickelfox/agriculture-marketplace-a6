
from django.urls import path, include

from . import views

urlpatterns = [
    path('role/', views.RoleView.as_view(), name='role'),
    path('role/<uuid:id>/', views.RoleView.as_view(), name='role'),

    path('weather/',views.WeatherApi.as_view(), name='weather')
]