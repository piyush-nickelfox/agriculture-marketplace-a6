from django.db import models
from apps.home.models import Role, CustomUserManager, BaseModel
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from rest_framework.exceptions import ValidationError

class User(AbstractBaseUser, PermissionsMixin, BaseModel):
    email = models.EmailField(verbose_name='email address', unique=True, null=False, blank=False)
    name = models.CharField(verbose_name='full name', max_length = 255)
    phone = models.CharField(max_length=255, unique = True)
    role = models.ForeignKey(Role, null = True, blank=True, on_delete = models.CASCADE)
    
    is_verified = models.BooleanField(default=False)
    is_approved = models.BooleanField(default=False)

    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['name', 'phone']

    objects = CustomUserManager()

    def __str__(self):
        return self.name

    @staticmethod
    def get_instance(data):
        try:
            return User.objects.get(**data)
        except:
            return ValidationError('User does not exists.')
    
    @staticmethod
    def create_instance(data):
        try:
            return User.objects.create_user(**data)
        except:
            return ValidationError('User does not exists.')
    
    @staticmethod
    def filter_instance(data):
        try:
            return User.objects.filter(**data)
        except:
            return ValidationError('No such user exists.')