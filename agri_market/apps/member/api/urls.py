
from django.urls import path, include

from . import views

from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView, TokenVerifyView

urlpatterns = [
    path('register/',views.RegisterView.as_view(), name='register'),

    path('login/', views.LoginView.as_view(), name='login'),
    path('refreshtoken/', TokenRefreshView.as_view(), name='refresh-token'),
    path('profile/', views.ProfileView.as_view(), name='profile'),
    path('logout/', views.LogoutView.as_view(), name='logout'),
    
    path('user/', views.UserView.as_view(), name='user'),
    path('user/<uuid:id>/', views.UserView.as_view(), name='user-detail'),
]