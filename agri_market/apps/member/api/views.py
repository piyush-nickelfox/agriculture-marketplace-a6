from logging import exception
from apps.member.api.permissions import IsAdminPermission, ProfileUpdatePermission
from apps.member.api.serializers import UserSerializers, RegisterSerializers, AdminUserSerializers
from apps.member.models import User
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework import status, views, generics, exceptions


class RegisterView(views.APIView):
    permission_classes = [AllowAny]

    def post(self, request, format=None):
        serializer = RegisterSerializers(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'message':'Successfully created new account.'}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LoginView(views.APIView):
    permission_classes = [AllowAny]

    def post(self, request):
        email = request.data['email']
        password = request.data['password']
        user = User.get_instance({'email':email})
        if user is None:
            raise exceptions.AuthenticationFailed('User not found.')
        if not user.check_password(password):
            raise exceptions.AuthenticationFailed('Incorrect password')
        if not user.is_approved:
            raise exceptions.AuthenticationFailed('Your profile is not yet approved. Please wait for 5-7 workings days.')
        refresh = RefreshToken.for_user(user)
        return Response({
            'message':'successfully logged in',
            'token':{
                'refresh': str(refresh),
                'access':  str(refresh.access_token)
            } 
        })


class LogoutView(generics.GenericAPIView):
   authentication_classes = [JWTAuthentication]
   permission_classes = [IsAuthenticated]

   def post(self, request):
        try:
            token = request.data.get('refresh_token')
            token_obj = RefreshToken(token)
            token_obj.blacklist()
            return Response(status=status.HTTP_200_OK)
        except Exception as e:
            return Response(status=status.HTTP_400_BAD_REQUEST)


class ProfileView(generics.RetrieveUpdateAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated & ProfileUpdatePermission]

    def get_queryset(self):
        user = self.request.user.id
        return user

    def get(self, request, format=None):
        user_data = User.get_instance({'id': self.get_queryset()})
        serializer = UserSerializers(user_data)
        return Response(serializer.data)

    def patch(self, request, format=None):
        user_data = User.get_instance({'id': self.get_queryset()})
        serializer = UserSerializers(user_data, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response({'message':'Successfully updated profile.'}, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UserView(views.APIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated & IsAdminPermission]

    def get(self, request, id=None, format=None):
        if id is not None:
            user = User.get_instance({'id': id})
            serializer = AdminUserSerializers(user)
        else:
            users = User.objects.all().exclude(id=request.user.id).exclude(role__isnull=True)
            serializer = AdminUserSerializers(users, many=True)
        return Response(serializer.data)

    def patch(self, request, id=None, format=None):
        if id is not None:
            user = User.get_instance({'id': id})
            serializer =  AdminUserSerializers(user, data=request.data, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response({'message':'User portal changes added.'}, status=status.HTTP_200_OK)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response({'error','No such user exists.'}, status=status.HTTP_404_NOT_FOUND)
        