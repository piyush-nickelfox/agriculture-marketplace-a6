from xml.etree.ElementTree import iselement
from rest_framework import serializers
from apps.member.models import User
from apps.home.models import Role
from rest_framework_simplejwt.tokens import RefreshToken, TokenError

class RegisterSerializers(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['name', 'email', 'phone', 'role', 'password']
        write_only_fields = ['password']

    def validate_phone(self, phone):
        if not phone.isdigit():
            raise serializers.ValidationError('Please enter a valid phone number.')
        if len(phone)!=10:
            raise serializers.ValidationError('Please enter a valid phone number only 10 digits.')
        return phone

    def validate(self, args):
        email = args.get('email', None)
        if User.objects.filter(email = email).exists():
            raise serializers.ValidationError({'Email':'This email already exists.'})
        phone = args.get('phone')
        if User.objects.filter(phone=phone).exists():
            raise serializers.ValidationError({'phone':'This phone number already exists.'})
        admin_role = Role.get_instance({'name':'admin'})
        role = args.get('role', None)
        if role is None or role==admin_role:
            raise serializers.ValidationError({'role':"Role cannot be None or admin."}) 
        return super().validate(args)

    def create(self, validated_data):
        password = validated_data.pop('password', None)
        instance = self.Meta.model(**validated_data)
        if password is not None:
            instance.set_password(password)
        instance.save()
        return instance


class UserSerializers(serializers.ModelSerializer):
    class Meta:
        model = User
        exclude = ['is_staff','is_superuser','groups','user_permissions','deleted_at','last_login','is_active']
        read_only_fields = ['__all__']
        write_only_fields = ['password','phone','name']

    def validate_phone(self, phone):
        if not phone.isdigit():
            raise serializers.ValidationError('Please enter a valid phone number.')
        if len(phone)!=10:
            raise serializers.ValidationError('Please enter a valid phone number only 10 digits.')
        return phone
    
    def validate_password(self, password):
        if password.isnumeric():
            raise serializers.ValidationError('Enter a strong password.')
        
    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.phone = validated_data.get('phone', instance.phone)
        if 'password' in validated_data:
            instance.set_password(validated_data['password'])
        instance.save()
        return instance


class AdminUserSerializers(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id','email','name', 'phone', 'role','is_approved','is_active']
        read_only_fields = ['id','email', 'phone','role']
    
    def update(self, instance, validated_data):
        instance.is_approved = validated_data.get('is_approved', instance.is_approved)
        instance.is_active = validated_data.get('is_active', instance.is_active)
        instance.save()
        return instance
 