from rest_framework import permissions
from apps.home.models import Role

admin_role = Role.get_instance({'name':'admin'}).id

class ProfileUpdatePermission(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS: 
            return True
        return request.user.id == obj.id     


class IsAdminPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        user = request.user
        return user.role.id == admin_role    

