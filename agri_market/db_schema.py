from django.db import models
import uuid
# Create your models here.
from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin

#---------Home------------------
class BaseModel(models.Model):
    
    class Meta:
        abstract = True

    id = models.UUIDField(primary_key=True, default = uuid.uuid4, unique = True, editable=False)
    created_at = models.DateTimeField(auto_now_add = True)
    updated_at = models.DateTimeField(auto_now = True)
    deleted_at = models.DateTimeField(blank = True, null = True)
    

#----------- User ----------------------------

class CustomUserManager(BaseUserManager):

    def create_user(self, email, name,  password, **extra_fields):
        if not email:
            raise ValueError(_('The Email must be set'))
        email = self.normalize_email(email)
        user = self.model(email=email, name=name, **extra_fields)
        user.set_password(password)
        user.is_staff = True
        user.save()
        return user

    def create_superuser(self, email, name, password, **extra_fields):
      
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError(_('Superuser must have is_staff=True.'))
        if extra_fields.get('is_superuser') is not True:
            raise ValueError(_('Superuser must have is_superuser=True.'))
        return self.create_user(email, name, password, **extra_fields)


class Role(BaseModel):
    name = models.CharField(max_length = 6, unique=True, null=False, blank=False)

    def __str__(self):
        return self.name
    
    
class User(AbstractBaseUser, PermissionsMixin, BaseModel):
    email = models.EmailField(verbose_name='email address', unique=True, null=False, blank=False)
    name = models.CharField(verbose_name='full name', max_length = 255)
    phone = models.CharField(max_length=255, unique = True)
    role = models.ForeignKey(Role, null = True, blank=True, on_delete = models.CASCADE)
    
    is_verified = models.BooleanField(default=False)
    is_approved = models.BooleanField(default=False)

    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['name']

    objects = CustomUserManager()

    def __str__(self):
        return self.name



#-----------Agriculture-----------
class Category(BaseModel):
    TYPE = (
        ('Crop', 'Crop'),
        ('Soil','Soil')
    )
    name = models.CharField(max_length=255)
    type = models.CharField(max_length=10, choices=TYPE, default='Crop')


class Commodity(BaseModel):
    CATEGORY = (
        ('Product','Product'),
        ('Service','Service')
    )
    name = models.CharField(max_length=255)
    description = models.TextField
    category = models.CharField(max_length=10, choices=CATEGORY)
    cost = models.DecimalField()

class Order(BaseModel):
    item = models.ForeignKey(Commodity, on_delete=models.CASCADE)
    farmer = models.ForeignKey(User, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField()
    price = models.DecimalField()
    is_deliverd = models.BooleanField(default=False)
    deliver_date = models.DateField(blank=True, null=True)

class Media(BaseModel):
    image = models.ImageField(upload_to='farmImages')

class Farm(BaseModel):
    farmer = models.ForeignKey(User, on_delete=models.CASCADE)
    length = models.DecimalField()
    breadth = models.DecimalField()
    purchased_on = models.DateField()
    soil = models.ForeignKey(Category, on_delete=models.CASCADE)
    images = models.ManyToManyField(Media, blank=True)

class Crop(BaseModel):
    CROP_STATUS_HISTROY = {
        'sowing':None,
        'seeding':None,
        'harvesting':None
    }
    farm = models.ForeignKey(Farm, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    type = models.ForeignKey(Category, on_delete=models.CASCADE)
    #sowing, seeding, harvesting
    status = models.JSONField(default=CROP_STATUS_HISTROY)


class Rating(BaseModel):
    RATE_SCALE = tuple((i,i) for i in range(11))
    admin = models.ForeignKey(User, related_name='admin', on_delete=models.CASCADE)
    farmer = models.ForeignKey(User, related_name='farmer', on_delete=models.CASCADE)
    rating = models.IntegerField(choices=RATE_SCALE, default=0)
     

class Market(BaseModel):
    farmer = models.ForeignKey(User, on_delete=models.CASCADE)
    crop = models.ForeignKey(Crop, on_delete=models.CASCADE)
    quantity = models.IntegerField()
    admin_cost = models.DecimalField()
    agent_cost = models.DecimalField()
    is_sold = models.BooleanField(default=False)
    sell_date = models.DateTimeField(blank=True, null=True)
    expire_date = models.DateTimeField(blank=True, null=True)
    

class Loan(BaseModel):
    approved_by = models.ForeignKey(User, on_delete=models.CASCADE)
    farmer = models.ForeignKey(User, on_delete=models.CASCADE)
    reason = models.TextField()
    amount = models.DecimalField()
    rate = models.DecimalField()
    tenure = models.IntegerField()
    is_approved = models.BooleanField(default=False)
    

class Bid(BaseModel):
    agent = models.ForeignKey(User, on_delete=models.CASCADE)
    farmer = models.ForeignKey(User, on_delete=models.CASCADE)
    item = models.ForeignKey(Market, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField()
    cost = models.DecimalField()
    is_finalized = models.BooleanField(default=False)


class MarketOrder(BaseModel):
    item = models.ForeignKey(Market, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField()
    cost = models.DecimalField()
    is_deliverd = models.BooleanField(default=False)
    deliver_date = models.DateField(blank=True, null=True)
    

